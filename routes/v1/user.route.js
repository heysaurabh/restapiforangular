const express = require('express');
const auth=require('../../middlewares/auth');
const validate=require('../../middlewares/validate');
const userValidation=require('../../validations/user.validation');
const userController=require('../../controllers/user.controller');

const router=express.Router();

router.route('/')
    .post(auth('manageUser'),validate(userValidation.createUser),userController.createUser)
    .get(auth('getUsers'),validate(userValidation.getUsers),userController.getUsers);

router.route('/:userId')
    .get(auth('getUser'),validate(userValidation.getUser),userController.getUser)
    .patch(auth('manageUser'),validate(userValidation.updateUser),userController.updateUser)
    .delete(auth('manageUser'),validate(userValidation.deleteUser),userController.deleteUser);

module.exports=router;





/*      ROUTE PAYLOAD REQUIRED FOR REQUEST TO FULFILL
const createUser={
    body:({
        email:Joi.string().required().email(),
        password:Joi.string().required().custom(password),
        name:Joi.string().required(),
        role:Joi.string().required().valid('user','admin')
    })
}
const getUsers={
    query:({
        name:Joi.string(),
        role:Joi.string(),
        sortBy:Joi.string(),
        limit:Joi.number().integer(),
        page:Joi.number().integer()
    })
}
const getUser={
    params:({
        userId:Joi.string().custom(objectId)
    })
}
const updateUser={
    params:({
        userId:Joi.required().custom(objectId)
    }),
    body:({
        email:Joi.string().required().email(),
        password:Joi.string().required().custom(password),
        name:Joi.string()
    }).min(1)
}
const deleteUser={
    params:({
        userId:Joi.string().custom(objectId)
    })
}
*/