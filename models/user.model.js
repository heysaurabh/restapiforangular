const mongoose = require('mongoose');
const validator=require('validator');
const bcrypt=require('bcrypt');
const { roles } = require('../config/roles');
const {paginate}=require('./plugins');
const userSchema=new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if (!validator.isEmail(value))
                throw new Error("Schema validation fail: Invalid email");
        }
    },
    password:{
        type:String,
        required:true,
        trim:true,
        minLength:8,
        validate(value) {
            if (!value.match(/\d/) || !value.match(/[a-zA-Z]/))
                throw new Error('Password must contain at least one letter and one number')
        }
    },
    role: {
        type: String,
        enum: roles,
        default: 'user',
    },
    isEmailverified:{
        type:Boolean,
        default:false
    }
},{timestamp:true});

//registering the plugin to this schema
userSchema.plugin(paginate);

//check if the email is taken already, and we need to exclude checking the current userid
userSchema.statics.isEmailTaken=async function(email,excludeUserId){
    const user=await this.findOne({email,_id:{$ne:excludeUserId}});
    return !!user;  //  !! converts the object to boolean
}
//function for checking if the password matches with the stored password
userSchema.methods.isPasswordMatch=async function(password){
    const user=this;
    return bcrypt.compare(password,user.password);
}
//here we are hashing the password before storing in the DB
userSchema.pre('save',async function (next){
    const user=this;
    if (user.isModified('password')){
        user.password=await bcrypt.hash(user.password,8);
    }
    next();
})

const User=mongoose.model('User',userSchema);
module.exports=User;