const objectId=(value,helper)=>{
    if(!value.match(/^[0-9a-fA-F]{24}$/)){
        return helpers.message('"{{#label}}" must be a valid mongo id');
    }
    return value;
}
const password=(value,helpers)=>{
    console.log("Custom Password validator run");
    if (value.length < 8) {
        return helpers.error('password must be at least 8 characters');
    }
    if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
        return helpers.error('password must contain at least 1 letter and 1 number');
    }
    return value;
}

module.exports={
    password,
    objectId
}