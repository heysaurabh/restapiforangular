const joi=require('joi');
const httpStatus=require('http-status');
const pick=require('../utils/pick');
const ApiError=require('../utils/ApiError');

//IMPORTANT: the main motive to wrap the main fn inside a outer fn is to pass more no of required parameters to the main fn,
//as we cannot change the fn signature, we need to wrap our main fn inside a outer fn

//here we are using function curring, we are returning a route middleware which will be called
//before the route handler automatically,
//this function takes the schema attribute and stores it in its lexical scope and is returned along with
//the function, which then validates the request, more at: https://expressjs.com/en/guide/using-middleware.html

const validate=(schema)=>(req,res,next)=>{
    console.log("Validate called")
    console.log(req.body);
    const validSchema=pick(schema,['params','query','body']);

    const object=pick(req,Object.keys(validSchema));
    //object will contain the required data to validate from the req object
    const {value,error}=joi.compile(validSchema)
        .prefs({errors:{label:'key'},abortEarly:true})
        .validate(object);

    if (error){
        console.log("error in validation")
        const errorMessage=error.details.map(dets=>dets.message).join(', ');
        console.log(errorMessage)
        return next(new ApiError(httpStatus.BAD_REQUEST,errorMessage));
    }
    //at last we assing all the valid values to the request
    Object.assign(req,value);
    return next();
}

module.exports=validate;